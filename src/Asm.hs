{-# LANGUAGE PatternSynonyms, DeriveGeneric, DeriveAnyClass, FlexibleContexts, NamedFieldPuns #-}
{-# LANGUAGE LambdaCase, NoMonomorphismRestriction, DataKinds, ViewPatterns, TypeApplications #-}
{-# LANGUAGE BlockArguments, OverloadedStrings, StandaloneDeriving, DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Asm where

import Interlude hiding (State, interact, fail)
import Data.Vector.Unboxed (Vector)
import qualified Data.Vector.Unboxed as V
import qualified Data.Vector.Unboxed.Mutable as MV
import Control.Effects
import Control.Effects.State
import Control.Effects.Reader
import Control.Monad.Fail
import Control.Evo
import Control.Monad.Trans.Maybe (runMaybeT)
import Data.Maybe (fromJust)

newtype Program = Program (Vector Int)
    deriving (Read, Show)

pattern Noop, Push, Pop, Dup, Add, Mult, Sub, Div, Jump, CJump, Pc, Swap, Unknown :: Int
pattern Interact :: Int -> Int
pattern Noop = 0
pattern Push = 1
pattern Pop = 2
pattern Dup = 3
pattern Add = 4
pattern Mult = 5
pattern Sub = 6
pattern Div = 7
pattern Jump = 8
pattern CJump = 9
pattern Pc = 10
pattern Swap = 11
pattern Interact n <- ((\n -> guard (n >= 100 && n < 200) >> Just n) -> Just n)
    where Interact n = n
pattern Unknown <- _
{-# COMPLETE Noop, Push, Pop, Dup, Add, Mult, Sub, Div, Jump, CJump, Pc, Swap, Interact, Unknown #-}

allInstructions :: [Int]
allInstructions = [0..11]

data Runtime m = Runtime
    { _outOfBounds :: Int -> Int -> m Int
    , _popEmpty :: m Int
    , _interact :: Int -> m ()
    , _unknownInstruction :: m ()
    , _divByZero :: m Int }
    deriving (Generic, Effect)

outOfBounds :: MonadEffect Runtime m => Int -> Int -> m Int
popEmpty :: MonadEffect Runtime m => m Int
interact :: MonadEffect Runtime m => Int -> m ()
unknownInstruction :: MonadEffect Runtime m => m ()
divByZero :: MonadEffect Runtime m => m Int
Runtime outOfBounds popEmpty interact unknownInstruction divByZero = effect

data CpuState = CpuState
    { pc :: Int
    , ticks :: Int
    , stack :: [Int] }

advancePc :: MonadEffect (State CpuState) m => m ()
advancePc = modifyState (\cpu -> cpu { pc = pc cpu + 1 })

seek :: MonadEffect (State CpuState) m => Int -> m ()
seek pc' = modifyState (\cpu -> cpu { pc = pc' })

readCode :: MonadEffects '[State CpuState, ReadEnv Program, Runtime] m => m Int
readCode = do
    Program prog <- readEnv
    CpuState pc _ _ <- getState
    case prog V.!? pc of
        Nothing -> outOfBounds pc (V.length prog)
        Just instr -> advancePc >> return instr

push :: MonadEffect (State CpuState) m => Int -> m ()
push n = do
    cpu <- getState
    setState (cpu { stack = n : stack cpu })

peek :: (MonadEffect (State CpuState) m, MonadEffect Runtime m) => m Int
peek = do
    cpu <- getState
    case stack cpu of
        [] -> popEmpty
        (top : _) -> return top

pop :: (MonadEffect (State CpuState) m, MonadEffect Runtime m) => m Int
pop = do
    cpu <- getState
    case stack cpu of
        [] -> popEmpty
        (top : rest) -> setState (cpu { stack = rest }) >> return top

stepProgram :: MonadEffects '[State CpuState, ReadEnv Program, Runtime] m => m ()
stepProgram = readCode >>= \case
    Noop -> return ()
    Push -> readCode >>= push
    Pop -> void pop
    Dup -> peek >>= push
    Add -> ((+) <$> pop <*> pop) >>= push
    Mult -> ((*) <$> pop <*> pop) >>= push
    Sub -> ((-) <$> pop <*> pop) >>= push
    Div -> do
        a <- pop
        b <- pop
        res <- 
            if b == 0 then divByZero
            else return (div a b)
        push res
    Jump -> pop >>= seek
    CJump -> do
        cond <- pop
        tgt <- pop
        when (cond > 0) (seek tgt)
    Pc -> (pc <$> getState) >>= push
    Swap -> do
        a <- pop
        b <- pop
        push a
        push b
    Interact n -> interact n
    Unknown -> unknownInstruction


interpret :: Monad m => Runtime (StateT CpuState m) -> Int -> Program -> m ()
interpret rtm maxTicks prog@(Program code) =
    implementStateViaStateT (CpuState 0 0 []) $ implement rtm $ implementReadEnv (return prog) $ go
    where
    go = do
        stepProgram
        st@CpuState{ticks} <- getState
        setState (st { ticks = ticks + 1 })
        unless (ticks > maxTicks || pc st == V.length code) go

crashOnlyRuntime :: MonadFail m => (Int -> RuntimeImplemented Runtime m ()) -> Runtime m
crashOnlyRuntime inter = rtm
    where
    rtm = Runtime
        { _outOfBounds = \_ _ -> fail "out of bounds"
        , _popEmpty = fail "pop empty"
        , _interact = implement rtm . inter
        , _unknownInstruction = fail "unknown instruction"
        , _divByZero = fail "div by zero" }

data InstructionArgumentInteraction
    = Instruction
    | Argument
    | Interaction

randomCode :: MonadRandom m => Int -> m Int
randomCode interactions = do
    instructionArgumentOrInteraction <-
        weighted [(Instruction, 3/6), (Argument, 2/6), (Interaction, 1/6)]
    case instructionArgumentOrInteraction of
        Argument -> uniform [-10..10]
        Instruction -> uniform allInstructions
        Interaction -> uniform [100..100 + interactions - 1]

randomProgram :: MonadRandom m => Int -> Int -> m Program
randomProgram len interactions = Program . V.fromList <$> replicateM len (randomCode interactions)

crossPrograms :: MonadRandom m => Program -> Program -> m Program
crossPrograms (Program prog1) (Program prog2) =
    Program <$> V.zipWithM (\a b -> uniform [a, b]) prog1 prog2

data TweakOrNew = Tweak | NewCode | RemoveCode

mutateProgram :: MonadRandom m => Int -> Program -> m Program
mutateProgram interactions (Program prog) = do
    prog' <- weighted [(Tweak, 8/10), (NewCode, 1/10), (RemoveCode, 1/10)] >>= \case
        Tweak
            | V.length prog > 0 -> do
                ofs <- uniform ([-3 .. -1] <> [1..3])
                idx <- getRandomR (0, V.length prog - 1)
                return (V.modify (\vec -> MV.modify vec (+ ofs) idx) prog)
            | otherwise -> return prog
        NewCode -> do
            newCodeSize <- randomSize
            Program newCode <- randomProgram newCodeSize interactions
            whereToInsert <- getRandomR (0, V.length prog - 1)
            return (V.concat [V.take whereToInsert prog, newCode, V.drop whereToInsert prog])
        RemoveCode -> do
            removeSize <- randomSize
            whereToRemove <- getRandomR (0, V.length prog - 1)
            return (V.take whereToRemove prog <> V.drop (whereToRemove + removeSize) prog)
    return (Program prog')

randomSize :: MonadRandom m => m Int
randomSize = weighted (fmap (\i -> (i + 1, ((2 / 3) ^ i))) [0..10])

printActivity :: MonadIO m => EvolutionActivity Program (Int, Int) -> m ()
printActivity (NewGenerationActivity gen) = do
    let (prog, score) = selectBest gen
    putStr (pshow score <> ": " <> show prog <> "\n" :: Text)
printActivity EnvironmentMutation = putText "Env mutation"
printActivity _ = return ()

deriving newtype instance MonadFail m => MonadFail (RuntimeImplemented e m)


evoHandler :: MonadRandom m => Evolution Program (Int, Int) m
evoHandler = EvolutionMethods newProg (mutateProgram interactions) crossPrograms eval (return ())
    where
    interactions = 1
    newProg = do
        randomProgram 10 interactions
    eval prog = do
        results <- forM [1..5] \i -> do
            res <- (interpret (crashOnlyRuntime (inters i)) 10000 prog >> getState @(Maybe Int))
                & implementStateViaStateT (Nothing :: Maybe Int)
                & runMaybeT
            return (case res of
                Nothing -> (0, 0)
                Just Nothing -> (1, 0)
                Just (Just n) -> (2, -abs(i ^ (6 - i) - n)))
        return (fromJust (foldl1May' (\(a, b) (c, d) -> (a + c, b + d)) results))
    inters i 100 = push i
    inters i 101 = push (6 - i)
    inters _ 102 = pop >>= setState . Just
    inters _ _ = fail "unknown interaction"

test = do
    iterativeEvolution
        & handleEvolution
            evoHandler
            (LogMethods printActivity)
        & implementReadEnv (return (GeneticConfiguration 10 20 30))