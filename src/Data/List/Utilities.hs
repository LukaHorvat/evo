module Data.List.Utilities where

import Interlude

pairwise :: [a] -> [(a, a)]
pairwise xs = zip xs (unsafeTail xs)

firstJust :: (a -> Maybe b) -> [a] -> Maybe b
firstJust f xs = join (find isJust (map f xs))
