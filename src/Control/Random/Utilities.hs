module Control.Random.Utilities where

import Interlude hiding (list)

uniformRandomN :: MonadRandom m => Int -> [a] -> m [a]
uniformRandomN n list
    | n > totalInList = error $ "Not enough elements in the list (" <> show totalInList <> ")\
                                \ to choose random " <> show n
    | otherwise       = getNext n totalInList list
    where totalInList = length list
          getNext 0        _         _        = return []
          getNext required remaining (x : xs) = do
              r <- getRandomR (1, remaining)
              if r <= required then do
                  rest <- getNext (required - 1) (remaining - 1) xs
                  return (x : rest)
              else getNext required (remaining - 1) xs
          getNext _        _         _        = error "Can't happen"
