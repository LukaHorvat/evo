{-# LANGUAGE TypeFamilies, FlexibleContexts, RecordWildCards, ScopedTypeVariables #-}
{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, ConstraintKinds, FunctionalDependencies #-}
{-# LANGUAGE UndecidableInstances, DataKinds, UndecidableSuperClasses, GADTs #-}
{-# LANGUAGE AllowAmbiguousTypes, TypeApplications, Rank2Types, DeriveGeneric, DeriveAnyClass #-}
{-# LANGUAGE LambdaCase, NoMonomorphismRestriction, TupleSections #-}
module Control.Evo where

import Interlude
import Control.Effects
import Control.Effects.Reader

import Control.Monad.Utilities
import Control.Random.Utilities

data Evolution u v m = EvolutionMethods
    { _newInitialUnit :: m u
    , _mutateUnit :: u -> m u
    , _crossUnits :: u -> u -> m u
    , _evaluateUnit :: u -> m v
    , _mutateEnvironment :: m () }
    deriving (Generic)
instance Effect (Evolution u v)


data Log a m = LogMethods
    { _log :: a -> m () }
    deriving (Generic)
instance Effect (Log a)

-- | A log of something that happens during evolution
data EvolutionActivity u v = MutationActivity { beforeMutationUnit  :: (u, v)
                                              , afterMutationUnit   :: (u, v) }
                           | CrossActivity { beforeCrossingUnits :: ((u, v), (u, v))
                                           , afterCrossingUnit   :: (u, v) }
                           | NewGenerationActivity { newGeneration :: [(u, v)] }
                           | SpeciesStackChange { newSpeciesStack :: [(Int, [(u, v)])] }
                           | EnvironmentMutation
                           deriving (Eq, Ord, Read, Show)

class
    ( MonadRandom m, Ord v
    , MonadEffects '[Evolution u v, Log (EvolutionActivity u v), ReadEnv GeneticConfiguration] m )
    => MonadEvolution u v m | m -> u v
instance
    ( MonadRandom m, Ord v
    , MonadEffects '[ReadEnv GeneticConfiguration] m )
    => MonadEvolution u v
        (RuntimeImplemented (Evolution u v)
        (RuntimeImplemented (Log (EvolutionActivity u v))
        m))
instance {-# OVERLAPPABLE #-}
    ( MonadEvolution u v m
    , MonadTrans t, MonadRandom (t m) )
    => MonadEvolution u v (t m)

newInitialUnit :: forall u v m. MonadEvolution u v m => m u
newInitialUnit = _newInitialUnit (effect @(Evolution u v))
mutateUnit :: forall u v m. MonadEvolution u v m => u -> m u
mutateUnit = _mutateUnit (effect @(Evolution u v))
crossUnits :: forall u v m. MonadEvolution u v m => u -> u -> m u
crossUnits = _crossUnits (effect @(Evolution u v))
evaluateUnit :: forall u v m. MonadEvolution u v m => u -> m v
evaluateUnit = _evaluateUnit (effect @(Evolution u v))
mutateEnvironment :: forall u v m. MonadEvolution u v m => m ()
mutateEnvironment = _mutateEnvironment (effect @(Evolution u v))
evolutionActivity :: forall u v m. MonadEvolution u v m => EvolutionActivity u v -> m ()
LogMethods evolutionActivity = effect

handleEvolution ::
    forall u v m a. Monad m =>
    Evolution u v m
    -> Log (EvolutionActivity u v) m
    -> RuntimeImplemented (Evolution u v) (RuntimeImplemented (Log (EvolutionActivity u v)) m) a -> m a
handleEvolution evoHandler logHandler =
    implement logHandler
    . implement (liftThrough evoHandler)

data GeneticConfiguration = GeneticConfiguration { tournamentPoolSize        :: Int
                                                 , persistedPoolSize         :: Int
                                                 , initialStalenessTolerance :: Int }
                                                 deriving (Eq, Ord, Read, Show)

initialPopulation :: MonadEvolution u v m => Int -> m [u]
initialPopulation = (`replicateM` newInitialUnit)

evaluatePopulation :: (MonadEvolution u v m) => [u] -> m [(u, v)]
evaluatePopulation = mapM (\u -> do
    v <- evaluateUnit u
    return (u, v))

selectBest :: Ord v => [(a, v)] -> (a, v)
selectBest = maximumBy (comparing snd)

tournamentSelectUnit :: (MonadRandom m, Ord v) => Int -> [(a, v)] -> m (a, v)
tournamentSelectUnit poolSize evaluatedPop =
    selectBest <$> uniformRandomN poolSize evaluatedPop

tournamentSelectPool :: (MonadRandom m, MonadEffect (ReadEnv GeneticConfiguration) m, Ord v)
                     => [(u, v)] -> m [(u, v)]
tournamentSelectPool evaluatedPop = do
    GeneticConfiguration{..} <- readEnv
    replicateM persistedPoolSize (tournamentSelectUnit tournamentPoolSize evaluatedPop)

crossPool :: ( MonadEvolution u v m )
          => [(u, v)] -> m [(u, v)]
crossPool pool = sequence $ crossAndLog <$> pool <*> pool

crossAndLog :: MonadEvolution u v m => (u, v) -> (u, v) -> m (u, v)
crossAndLog a@(u1, _) b@(u2, _) = do
    newUnit  <- crossUnits u1 u2
    newValue <- evaluateUnit newUnit
    evolutionActivity (CrossActivity (a, b) (newUnit, newValue))
    return (newUnit, newValue)


mutatePool :: ( MonadEvolution u v m )
           => [(u, v)] -> m [(u, v)]
mutatePool = mapM mutateAndLog
    where mutateAndLog a@(u, _) = do
              newUnit  <- mutateUnit u
              newValue <- evaluateUnit newUnit
              evolutionActivity (MutationActivity a (newUnit, newValue))
              return (newUnit, newValue)

reevaluate :: MonadEvolution u v m => [(u, v)] -> m [(u, v)]
reevaluate = mapM (\u -> (u, ) <$> evaluateUnit u) . map fst

advancePopulation :: ( MonadEvolution u v m )
                  => [(u, v)] -> m [(u, v)]
advancePopulation evaluatedPop = do
    let alpha = selectBest evaluatedPop
    selected <- (alpha :) <$> tournamentSelectPool evaluatedPop
    reevaluated <- reevaluate selected
    crossed <- crossPool selected
    mutated <- mutatePool selected
    return (reevaluated <> crossed <> mutated)

iterativeEvolution :: MonadEvolution u v m => m ()
iterativeEvolution = do
    GeneticConfiguration{..} <- readEnv
    initPop :: [u] <- initialPopulation persistedPoolSize
    evaluatedPop <- evaluatePopulation initPop
    void $ loopM (evaluatedPop, 1 :: Int) $ \(pop, iter) -> do
        newPop <- advancePopulation pop
        evolutionActivity (NewGenerationActivity newPop)
        let staleness = if maximum (map snd pop) == maximum (map snd newPop) then iter + 1 else 1
        newStaleness <- if staleness > 20 then do
            evolutionActivity EnvironmentMutation
            mutateEnvironment
            return 1
            else return staleness
        return (newPop, newStaleness)

evolveSpecies :: (MonadEvolution u v m, Show v) => [(u, v)] -> Int -> m [(u, v)]
evolveSpecies evaluatedPop stalenessTolerance = do
    (_, spec) <- whileM (0, evaluatedPop) (\(staleness, _) -> return (staleness < stalenessTolerance)) $
        \(staleness, pop) -> do
        let (_, lastBestScore) = selectBest pop
        newPop <- advancePopulation pop
        evolutionActivity (NewGenerationActivity newPop)
        let (_, newBestScore) = selectBest newPop
        if lastBestScore == newBestScore then
            return (staleness + 1, newPop)
        else return (0, newPop)
    return spec

mergeSpecies :: Show v => MonadEvolution u v m
             => Int -> [(u, v)] -> [(u, v)] -> m (Int, [(u, v)])
mergeSpecies newLvl spec1 spec2 = do
    let alpha1 = selectBest spec1
    champions1 <- (alpha1 :) <$> tournamentSelectPool spec1
    let alpha2 = selectBest spec2
    champions2 <- (alpha2 :) <$> tournamentSelectPool spec2
    let champs = champions1 <> champions2
    crossed <- sequence $ crossAndLog <$> (champions1 <> champions1) <*> (champions2 <> champions2)
    mutated <- mutatePool crossed
    newSpec <- evolveSpecies (champs <> crossed <> mutated) newLvl
    return (newLvl, newSpec)


speciesEvolution :: Show v => MonadEvolution u v m => m ()
speciesEvolution =
    void $ loopM [] $ \case
        (lvl1, spec1) : (lvl2, spec2) : rest | lvl1 == lvl2 -> do
            let newLvl = lvl1 + 1
            newSpec :: (Int, [(u, v)]) <- mergeSpecies newLvl spec1 spec2
            evolutionActivity (SpeciesStackChange (newSpec : rest))
            return (newSpec : rest)
        rest -> do
            GeneticConfiguration{..} <- readEnv
            initPop <- initialPopulation persistedPoolSize
            evaluatedPop <- evaluatePopulation initPop
            newSpec <- evolveSpecies evaluatedPop initialStalenessTolerance
            evolutionActivity (SpeciesStackChange ((initialStalenessTolerance, newSpec) : rest))
            return ((initialStalenessTolerance, newSpec) : rest)
