module Control.Monad.Utilities where

import Interlude

loopM :: Monad m => a -> (a -> m a) -> m a
loopM i f = do
    a <- f i
    loopM a f

whileM :: Monad m => a -> (a -> m Bool) -> (a -> m a) -> m a
whileM i c f = do
    satisfied <- c i
    if satisfied then do
        a <- f i
        whileM a c f
    else return i
