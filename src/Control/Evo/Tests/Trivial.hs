{-# LANGUAGE NoMonomorphismRestriction, FlexibleContexts, ScopedTypeVariables, DataKinds, GADTs #-}
{-# LANGUAGE TypeApplications #-}
module Control.Evo.Tests.Trivial where

import Interlude hiding (State)

import Control.Evo
import Control.Effects.Reader
import Control.Effects.State
import Control.Monad.Utilities
import Data.IORef

mutateNumber :: MonadRandom m => Double -> m Double
mutateNumber x = (x +) <$> getRandomR (-1, 1)

crossNumbers :: Monad m => Double -> Double -> m Double
crossNumbers x y = return $ (x + y) / 2

evaluateNumber :: Monad m => Double -> m Double
evaluateNumber x = return $ - (abs (100 - x))

randomNumber :: MonadRandom m => m Double
randomNumber = getRandomR (0, 200)

evoHandler = EvolutionMethods randomNumber mutateNumber crossNumbers evaluateNumber (return ())

printActivity :: (MonadEffect (State Double) m, MonadIO m)
    => EvolutionActivity Double Double -> m ()
printActivity (NewGenerationActivity gen) = do
    currBest <- getState @Double
    let maybeNewBest = snd (selectBest gen)
    when (maybeNewBest > currBest) (setState maybeNewBest)
-- printActivity (SpeciesStackChange specs) = forM_ specs $ \(lev, pop) -> do
--     -- putStrLn $ show (lev - 10) <> " " <> show (selectBest pop)
printActivity _ = return ()


test :: IO ()
test = do
    currBest <- newIORef (-100)
    forkIO $ void $ loopM (0, []) $ \(t, lst) -> do
        when (t `mod` 10  == 0) (writeFile "timing.txt" (show lst))
        cb <- readIORef currBest
        threadDelay 1000000
        return (t + 1, (t, cb) : lst)
    speciesEvolution
        & handleEvolution evoHandler (LogMethods printActivity)
        & implementReadEnv (return (GeneticConfiguration 10 20 10))
        & implement (StateMethods @Double (readIORef currBest) (writeIORef currBest))
