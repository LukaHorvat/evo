{-# LANGUAGE FlexibleContexts, ScopedTypeVariables, RecordWildCards, OverloadedStrings, TypeApplications #-}
{-# LANGuAGE TupleSections, ViewPatterns, FlexibleInstances, MultiParamTypeClasses, TypeFamilies #-}
module Control.Evo.Distributed where

import Interlude hiding (State)

import Control.Effects.State
import Control.Effects.Reader
import Network.AMQP
import Control.Evo
import Data.Tree

data Distributed u v = Queued | Calculated [(u, v)] | Free
                       deriving (Eq, Ord, Read, Show)

data TreeDirection = GoLeft | GoRight deriving (Eq, Ord, Read, Show)

type TreeIndex = [TreeDirection]

type TreeSize = Int

data SpeciesTree u v = InitialSpecies (Distributed u v)
                     | CrossedSpecies (Distributed u v) (SpeciesTree u v) (SpeciesTree u v)
                       deriving (Eq, Ord, Read, Show)

data EvolutionTask u v = InitiateSpecies
                       | CrossSpecies
                       { species1             :: [(u, v)]
                       , species2             :: [(u, v)]
                       , speciesLevel         :: Int }
                         deriving (Eq, Ord, Read, Show)

getTreeLabel :: SpeciesTree u v -> Distributed u v
getTreeLabel (InitialSpecies lab)     = lab
getTreeLabel (CrossedSpecies lab _ _) = lab

treeHeight :: SpeciesTree u v -> TreeSize
treeHeight (InitialSpecies _) = 0
treeHeight (CrossedSpecies _ l _) = treeHeight l + 1

consIndex :: TreeDirection -> (TreeIndex, EvolutionTask u v) -> (TreeIndex, EvolutionTask u v)
consIndex dir = first (dir :)

findAllFree :: SpeciesTree u v -> [(TreeIndex, EvolutionTask u v)]
findAllFree (InitialSpecies Free) = [([], InitiateSpecies)]
findAllFree (CrossedSpecies Free lt@(getTreeLabel -> Calculated left)
                                         (getTreeLabel -> Calculated right)) =
    [([], CrossSpecies left right (treeHeight lt + 1))]
findAllFree (CrossedSpecies Free lt rt) = ls <> rs
    where ls  = fmap (first (GoLeft :)) (findAllFree lt)
          rs  = fmap (first (GoRight :)) (findAllFree rt)
findAllFree _ = []

freeTree :: Int -> SpeciesTree u v
freeTree 0 = InitialSpecies Free
freeTree n = CrossedSpecies Free subtree subtree
    where subtree = freeTree (n - 1)

forkNewBranch :: forall u v m proxy. MonadEffect (State (SpeciesTree u v)) m
              => proxy (u, v) -> Int -> m ()
forkNewBranch _ height =
    modifyState (\(t :: SpeciesTree u v) -> CrossedSpecies Free t (freeTree height) )

enqueueOne :: TreeIndex -> SpeciesTree u v -> SpeciesTree u v
enqueueOne [] (InitialSpecies Free) = InitialSpecies Queued
enqueueOne [] (CrossedSpecies Free l r) = CrossedSpecies Queued l r
enqueueOne (GoLeft  : xs) (CrossedSpecies Free l r) = CrossedSpecies Free (enqueueOne xs l) r
enqueueOne (GoRight : xs) (CrossedSpecies Free l r) = CrossedSpecies Free l (enqueueOne xs r)
enqueueOne idx _ = error $ "Invalid index " <> pshow idx <> " to enqueue"

enqueue :: forall u v m proxy. MonadEffect (State (SpeciesTree u v)) m
        => proxy (u, v) -> [TreeIndex] -> m ()
enqueue _ indices = forM_ indices $ \idx ->
    modifyState (enqueueOne idx :: SpeciesTree u v -> SpeciesTree u v)

getNTasks :: forall u v m. (MonadEffect (State (SpeciesTree u v)) m)
          => Int -> m [(TreeSize, TreeIndex, EvolutionTask u v)]
getNTasks n = do
    speciesTree :: SpeciesTree u v <- getState
    let treeSize = treeHeight speciesTree
        addSize (i, t) = (treeSize, i, t)
        free = findAllFree speciesTree
        sorted = sortBy (comparing (length . fst)) free
        lengthFree = length free
        proxy = Proxy :: Proxy (u, v)
    if lengthFree >= n then do
        let res = take n sorted
        enqueue proxy (map fst res)
        return (map addSize res)
    else do
        enqueue proxy (map fst free)
        forkNewBranch proxy treeSize
        getNTasks n

placeSpecies :: TreeIndex -> [(u, v)] -> SpeciesTree u v -> SpeciesTree u v
placeSpecies [] sp (InitialSpecies Queued) = InitialSpecies (Calculated sp)
placeSpecies [] sp (CrossedSpecies Queued l r) = CrossedSpecies (Calculated sp) l r
placeSpecies (GoLeft  : xs) sp (CrossedSpecies s l r) = CrossedSpecies s (placeSpecies xs sp l) r
placeSpecies (GoRight : xs) sp (CrossedSpecies s l r) = CrossedSpecies s l (placeSpecies xs sp r)
placeSpecies idx _ _ = error $ "Invalid index " <> pshow idx <> " to place species at"

processResult :: forall u v m. MonadEffect (State (SpeciesTree u v)) m
              => TreeSize -> TreeIndex -> [(u, v)] -> m ()
processResult treeSize treeIndex spec = do
    tree :: SpeciesTree u v <- getState
    let wholeIdx = replicate (treeHeight tree - treeSize) GoLeft <> treeIndex
    setState (placeSpecies wholeIdx spec tree)

taskToMessage :: (Show u, Show v) => (TreeSize, TreeIndex, EvolutionTask u v) -> Message
taskToMessage task = newMsg { msgBody = pshow task }

queueSize :: Int
queueSize = 1

fillQueue :: forall u v m proxy.
             ( MonadEffect (State (SpeciesTree u v)) m
             , MonadIO m
             , Show u, Show v)
          => proxy (u, v) -> Channel -> m ()
fillQueue _ chan = do
    (_, filled, _) <- liftIO $ declareQueue chan newQueue { queueName = "taskQueue" }
    tasks :: [(TreeSize, TreeIndex, EvolutionTask u v)] <- getNTasks (max 0 (queueSize - filled))
    liftIO $ forM_ tasks $ \task@(s, i, _) -> do
        let specLevel = s - length i
        putText $ "Sending " <> pshow specLevel <> " level task"
        publishMsg chan "evoExchange" "taskKey" (taskToMessage task)

printTree :: (MonadIO m, Show v, Ord v) => SpeciesTree u v -> m ()
printTree = liftIO . appendFile "serverout.txt" . toS . drawTree . specTreeToTree
    where
    showDistr Free = "Free"
    showDistr Queued = "Queued"
    showDistr (Calculated spec) = "Calculated (" <> show (snd (selectBest spec)) <> ")"
    specTreeToTree (InitialSpecies dis) = Node (showDistr dis) []
    specTreeToTree (allFree -> True) = Node "Free (...)" []
    specTreeToTree (CrossedSpecies c@(Calculated _) _ _) = Node (showDistr c) []
    specTreeToTree (CrossedSpecies dis l r) = Node (showDistr dis) (specTreeToTree <$> [l, r])
    allFree (InitialSpecies Free) = True
    allFree (CrossedSpecies Free l r) = allFree l && allFree r
    allFree _ = False

processMessage :: forall u v m proxy.
                  ( MonadIO m, MonadEffect (State (SpeciesTree u v)) m
                  , Read u, Read v, Show u, Show v, Ord v )
               => proxy (u, v) -> (Message, Envelope) -> m ()
processMessage _ (msg, env) = do
    liftIO $ appendFile "serverout.txt" $ "Received " <> pshow (treeSize - length treeIndex) <> " level species:"
    liftIO $ appendFile "serverout.txt" $ pshow best
    processResult treeSize treeIndex res
    liftIO (ackEnv env)
    printTree =<< getState @(SpeciesTree u v)
    where (treeSize, treeIndex, res :: [(u, v)]) = pread (msgBody msg)
          best = unsafeHead res

distributedSpeciesEvolution :: forall u v proxy.
                               ( Show u, Show v, Read u, Read v, Ord v )
                            => proxy (u, v) -> IO ()
distributedSpeciesEvolution proxy = do
    chan <- getChannel
    let initialTree = InitialSpecies Free :: SpeciesTree u v
    implementStateViaStateT initialTree $ forever $ do
        fillQueue proxy chan
        maybeMsg <- liftIO $ getMsg chan Ack "resultQueue"
        mapM_ (processMessage proxy) maybeMsg
        liftIO (threadDelay 1000000)

processTask :: forall m u v. Show v => MonadEvolution u v m
            => TreeSize -> TreeIndex -> EvolutionTask u v -> m (TreeSize, TreeIndex, [(u, v)])
processTask s i InitiateSpecies = do
    GeneticConfiguration{..} <- readEnv
    initPop <- initialPopulation persistedPoolSize
    evaluatedPop <- evaluatePopulation initPop
    spec <- evolveSpecies evaluatedPop initialStalenessTolerance
    return (s, i, spec)
processTask s i CrossSpecies{..} = do
    GeneticConfiguration{..} <- readEnv
    (_, newSpec) <- mergeSpecies (initialStalenessTolerance + speciesLevel) species1 species2
    return (s, i, newSpec)

processTaskMessage :: forall u v m.
                      ( Read u, Read v, Show u, Show v
                      , MonadIO m, MonadEvolution u v m )
                   => Channel -> (Message, Envelope) -> m ()
processTaskMessage chan (msg, env) = do
    res <- processTask treeSize treeIndex tsk
    liftIO $ ackEnv env
    void $ liftIO $ publishMsg chan "evoExchange" "resultKey" (newMsg { msgBody = pshow res })
    where (treeSize, treeIndex, tsk :: EvolutionTask u v) = pread (msgBody msg)

distributedSpeciesEvolutionClient :: ( Read u, Read v, Show u, Show v
                                     , MonadIO m, MonadEvolution u v m )
                                  => m ()
distributedSpeciesEvolutionClient = do
    chan <- getChannel
    forever $ do
        maybeMsg <- liftIO $ getMsg chan Ack "taskQueue"
        mapM_ (processTaskMessage chan) maybeMsg
        liftIO $ threadDelay 1000

getChannel :: MonadIO m => m Channel
getChannel = liftIO $ do
    conn <- openConnection "localhost" "/" "guest" "guest"
    chan <- openChannel conn
    declareExchange chan newExchange { exchangeName = "evoExchange", exchangeType = "direct" }
    void $ declareQueue chan newQueue { queueName = "taskQueue" }
    void $ declareQueue chan newQueue { queueName = "resultQueue" }
    bindQueue chan "taskQueue" "evoExchange" "taskKey"
    bindQueue chan "resultQueue" "evoExchange" "resultKey"
    return chan

-- distributedSpeciesEvolution
--     :: forall m u v proxy.
--        ( Ord v, MonadRandom m, MonadEffect (EvolutionActivity u v) m
--        , MonadEffect (EvaluateUnit u v) m, MonadEffect (CrossUnits u) m
--        , MonadEffect (MutateUnit u) m, MonadEffect (NewInitialUnit u) m )
--     => proxy (u, v) -> Int -> Int -> m ()
-- distributedSpeciesEvolution _ _ tournamentPoolSize persistedPoolSize =
--     void $ loopM [] $ \specList -> case specList of
--         (lvl1, spec1) : (lvl2, spec2) : rest | lvl1 == lvl2 -> do
--             let newLvl = lvl1 + 1
--             evolutionActivity (SpeciesStackChange (newLvl : map fst rest) :: EvolutionActivity u v)
--             newSpec :: (Int, [(u, v)]) <-
--                 mergeSpecies tournamentPoolSize persistedPoolSize newLvl spec1 spec2
--             return (newSpec : rest)
--         rest -> do
--             initPop <- initialPopulation persistedPoolSize
--             evaluatedPop <- evaluatePopulation initPop
--             evolutionActivity (SpeciesStackChange (10 : map fst rest) :: EvolutionActivity u v)
--             newSpec <- evolveSpecies tournamentPoolSize persistedPoolSize evaluatedPop 10
--             return ((10, newSpec) : rest)
