{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE ViewPatterns #-}
module Applications where

import Interlude
import Text.Regex.PCRE.Heavy
import qualified Data.Text as Text
import Codec.Picture
import Data.List (partition)

englishGermanRegex :: Regex
englishGermanRegex = [re|^((0|(0|(([\ -d]|([f-w]|y)))*))|((([\&-y]|y))*([Q-c](.(.s*)?)?|.((y|s.((y|x)|s))|o(,|(([\&-y]|s))*)))|(([\ -d]|x*)|y)(c(([\&-y]|x))*)?))$|]

makeImage :: [Char] -> [Bool] -> IO ()
makeImage fp list = do
    let (_, img) = generateFoldImage 
            (\(b : bs) x y ->
                if
                    | x < 10 || x > 790 || y < 10 || y > 440 ->
                        (b : bs, PixelRGB8 80 80 80)
                    | b ->
                        (bs, PixelRGB8 200 200 200)
                    | otherwise ->
                        (bs, PixelRGB8 80 80 80)
            ) 
            list 
            800
            450
    writePng fp img


corpusTest :: IO ()
corpusTest = do
    eng <- Text.lines <$> readFile "english_big_lower"
    ger <- Text.lines <$> readFile "german_big_lower"
    let engCorrect = length (filter (=~ englishGermanRegex) eng)
        engTotal = length eng
        engPercent = floor ((fromIntegral engCorrect / fromIntegral engTotal :: Double) * 100) :: Int
        gerTotal = length ger
        gerCorrect = gerTotal - length (filter (=~ englishGermanRegex) ger)
        gerPercent = floor ((fromIntegral gerCorrect / fromIntegral gerTotal :: Double) * 100) :: Int
    putText $ "English: " <> show engCorrect <> "/" <> show engTotal <> " correct (" <> show engPercent <> "%)"
    putText $ "German: " <> show gerCorrect <> "/" <> show gerTotal <> " correct (" <> show gerPercent <> "%)"
    makeImage "eng_pix_map.png" (map (=~ englishGermanRegex) eng)
    makeImage "ger_pix_map.png" (map (=~ englishGermanRegex) ger)

namesTest :: IO ()
namesTest = do
    eng <- Text.lines <$> readFile "eng_names"
    ger <- Text.lines <$> readFile "ger_names"
    let engCorrect = length (filter (=~ englishGermanRegex) eng)
        engTotal = length eng
        engPercent = floor ((fromIntegral engCorrect / fromIntegral engTotal :: Double) * 100) :: Int
        gerTotal = length ger
        gerCorrect = gerTotal - length (filter (=~ englishGermanRegex) ger)
        gerPercent = floor ((fromIntegral gerCorrect / fromIntegral gerTotal :: Double) * 100) :: Int
    putText $ "English: " <> show engCorrect <> "/" <> show engTotal <> " correct (" <> show engPercent <> "%)"
    putText $ "German: " <> show gerCorrect <> "/" <> show gerTotal <> " correct (" <> show gerPercent <> "%)"

commonTest :: IO ()
commonTest = do
    eng <- Text.lines <$> readFile "english_common"
    let engCorrect = length (filter (=~ englishGermanRegex) eng)
        engTotal = length eng
        engPercent = floor ((fromIntegral engCorrect / fromIntegral engTotal :: Double) * 100) :: Int
    putText $ "English: " <> show engCorrect <> "/" <> show engTotal <> " correct (" <> show engPercent <> "%)"



fakeTest :: IO ()
fakeTest = do
    eng <- Text.lines <$> readFile "english_fake"
    ger <- Text.lines <$> readFile "german_fake"
    let engCorrect = length (filter (=~ englishGermanRegex) eng)
        engTotal = length eng
        engPercent = floor ((fromIntegral engCorrect / fromIntegral engTotal :: Double) * 100) :: Int
        gerTotal = length ger
        gerCorrect = gerTotal - length (filter (=~ englishGermanRegex) ger)
        gerPercent = floor ((fromIntegral gerCorrect / fromIntegral gerTotal :: Double) * 100) :: Int
    putText $ "English: " <> show engCorrect <> "/" <> show engTotal <> " correct (" <> show engPercent <> "%)"
    putText $ "German: " <> show gerCorrect <> "/" <> show gerTotal <> " correct (" <> show gerPercent <> "%)"
    forM_ eng $ \wd ->
        if wd =~ englishGermanRegex then putStr (wd <> " ")
        else putStr ("\\sout{" <> wd <> "} ")
    putText ""
    forM_ ger $ \wd ->
        if not (wd =~ englishGermanRegex) then putStr (wd <> " ")
        else putStr ("\\sout{" <> wd <> "} ")

textToSentences :: Text -> [[Text]]
textToSentences =
    fmap Text.words
    . Text.splitOn "."
    . Text.replace "\n" " "
    . Text.replace "!" " " 
    . Text.replace "?" " " 
    . Text.replace "," " "
    . Text.replace "(" " "
    . Text.replace ")" " " 
    . Text.replace "ä" "a" 
    . Text.replace "ö" "o" 
    . Text.replace "ü" "u" 
    . Text.replace "ß" "ss" 
    . Text.map toLower 

testSentence :: [Text] -> Bool
testSentence sent
    | ratio > 0.64 = True
    | otherwise = False
    where
    (length -> engWords, length -> gerWords) = partition (=~ englishGermanRegex) sent
    ratio = fromIntegral engWords / fromIntegral (engWords + gerWords) :: Double

textTest :: IO ()
textTest = do
    eng <- textToSentences <$> readFile "english_text"
    ger <- textToSentences <$> readFile "german_text"
    let engCorrect = length (filter testSentence eng)
        engTotal = length eng
        engPercent = floor ((fromIntegral engCorrect / fromIntegral engTotal :: Double) * 100) :: Int
        gerTotal = length ger
        gerCorrect = gerTotal - length (filter testSentence ger)
        gerPercent = floor ((fromIntegral gerCorrect / fromIntegral gerTotal :: Double) * 100) :: Int
    putText $ "English: " <> show engCorrect <> "/" <> show engTotal <> " correct (" <> show engPercent <> "%)"
    putText $ "German: " <> show gerCorrect <> "/" <> show gerTotal <> " correct (" <> show gerPercent <> "%)"
    forM_ eng $ \sent ->
        if testSentence sent then putStrLn (Text.unwords sent <> ".")
        else putStrLn ("\\sout{" <> Text.unwords sent <> ".}")
    forM_ ger $ \sent ->
        if not (testSentence sent) then putStrLn (Text.unwords sent <> ".")
        else putStrLn ("\\sout{" <> Text.unwords sent <> ".}")