module Main where

import Interlude

import Control.Evo.Tests.Regex
import qualified Asm as Asm

main :: IO ()
main = do
    as <- getArgs
    case as of
        ["server"] -> testDistributedServer
        ["iter"] -> testIterative
        ["spec"] -> test
        ["asm"] -> Asm.test
        _ -> testDistributed
